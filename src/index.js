import React from 'react';
import { Provider } from 'react-redux'; 
import ReactDOM from 'react-dom';
import './index.scss';
import App from './components/App/App';
 
ReactDOM.render(<App />, document.getElementById('root'))

