import RedStripe from '../RedStripe/RedStripe';
import './HotDirections.scss'
import BackgraundSecondSection from "../../assets/BackgraundSecondSection.jpeg";

export default function HotDirections () {
    return (
        <div className="hotDirections-conteiner"  style={{backgroundImage: `url(${BackgraundSecondSection})`
          }}>
           <p className="hotDirections-titel">Горячие направления</p>
           <RedStripe />
           <p className="hotDirections-text">Первое место для поиска экологически чистого отдыха</p>
        </div>
    );
}