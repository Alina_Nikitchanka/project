import './StatisticsSection.scss';

export default function StatisticsSection() {
    return (
        <div className="statisticsSection-conteiner">
            <div className="statisticsSection-section">
                <div className="statisticsSection-block">
                    <p className="statisticsSection-title">горячие направления</p>
                    <p className="statisticsSection-numbers">37</p>
                </div>
                <div className="statisticsSection-block">
                    <p className="statisticsSection-title">довольные клиенты</p>
                    <p className="statisticsSection-numbers">677</p>
                </div>
                <div className="statisticsSection-block">
                    <p className="statisticsSection-title">чашки кофе</p>
                    <p className="statisticsSection-numbers">87</p>
                </div>
                <div className="statisticsSection-block">
                    <p className="statisticsSection-title">посетили страны</p>
                    <p className="statisticsSection-numbers">107</p>
                </div>
            </div>
        </div>
    );
}