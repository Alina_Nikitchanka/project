import './SvgPlusText.scss';
import SvgBlock from '../SvgBlock/SvgBlock';
import Astronaut from '../Svg/Astronaut.svg'


export default function SvgPlusText() {
    return (
        <div className="svg-block">
            <SvgBlock svg={Astronaut} title="Типы праздников" />
            <SvgBlock title="Путеводители" />
            <SvgBlock title="Экстремальные туры" />
            <SvgBlock title="Отпуск мечты" />
        </div>
    );
}

//////svg???