import './Button.scss';

export default function Button({ text }) {
    return (
        <button className="btn"><a href="#"></a>{text}</button>
    );
}
