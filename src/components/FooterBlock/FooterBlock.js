import './FooterBlock.scss';

export default function FooterBlock() {
    return (
        <div className="footerBlock-conteiner">
            <div>
                <p className="footerBlock-title">Заголовок</p>
                <p className="footerBlock-text">Образец текста нижнего колонтитула</p>
            </div>
        </div>
    );
}