import { Fragment } from 'react';
import Adventure from '../Adventure/Adventure';
import SvgPlusText from '../SvgPlusText/SvgPlusText';
import HotDirections from '../HotDirections/HotDirections'
import Country from '../Country/Country'
import AmazingPlaces from '../AmazingPlaces/AmazingPlaces'
import EmailSection from '../EmailSection/EmailSection'
import DreamsSection from '../DreamsSection/DreamsSection'
import StatisticsSection from '../StatisticsSection/StatisticsSection'
import Footer from '../Footer/Footer'
import './App.scss';


function App() {
  return (
    <Fragment>
      <header className='container'>Туристическое агентство</header>
      <main className='container'>
        <Adventure/>
        <SvgPlusText/>
        <HotDirections />
        <Country />
        <AmazingPlaces />
        <EmailSection />
        <DreamsSection />
        <StatisticsSection />
      </main>
      <Footer />
    </Fragment>
  );
}

export default App;

