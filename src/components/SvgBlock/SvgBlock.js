import RedStripe from '../RedStripe/RedStripe';
import './SvgBlock.scss'


export default function SvgBlock({title}) {
    return (
        <div className="svg-conteiner">
            <div className="svg"></div>
            <p className="svg_title">{title}</p>
            <p className="svg_text">
                Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam.
            </p>
           <RedStripe />
        </div>
    );
}