import FooterBlock from '../FooterBlock/FooterBlock'
import './Footer.scss';

export default function Footer() {
    return (
        <div className="footer-conteiner">
            <div className="footer-firstBlock">
              <FooterBlock />
              <FooterBlock />
              <FooterBlock />
            </div>
            <div className="footer-secondBlock"> Туристическая компания </div>
        </div>
    );
}