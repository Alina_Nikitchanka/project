import Button from '../Button/Button';
import './DreamsSection.scss';
import BackgraudFog from "../../assets/BackgraudFog.jpeg";
import RedStripe from '../RedStripe/RedStripe';

export default function DreamsSection() {
    return (
        <div className="dreamsSection-conteiner" style={{
            backgroundImage: `url(${BackgraudFog})`
        }}>
            <div className="dreamsSection-conteiner-block">
                <p className="dreamsSection-title">Помогая мечтам исполняться</p>
                <RedStripe />
                <p className="dreamsSection-text">Вы можете получить от нас столько помощи, сколько захотите. Тебе решать. Наконец, вы должны знать, что мы относимся к местным жителям с уважением и справедливостью. Это окупается загрузкой ведра, потому что заботливые местные жители позволяют
                    ближе к их культуре, их людям и их природе. Что хорошо для них и хорошо для вас. Кроме того, если вы хотите, при бронировании отпуска мы оплатим однодневную поездку для малообеспеченного ребенка из развивающейся страны.
                    в игровой парк, гору или музей и т. д. Мы называем это ответственным туризмом.</p>
                <Button text="связаться" />
            </div>
        </div>
    );
}