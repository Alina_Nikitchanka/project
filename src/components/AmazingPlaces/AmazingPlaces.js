import RedStripe from '../RedStripe/RedStripe';
import './AmazingPlaces.scss';
import Mountains from '../../assets/Mountains.jpeg'
import Venice from '../../assets/Venice.jpeg'
import RoadAndForest from '../../assets/RoadAndForest.jpeg'
import RoadAndFog from '../../assets/RoadAndFog.jpeg'
import Road from '../../assets/Road.jpeg'
import Forest from '../../assets/Forest.jpeg'
import Waterfall from '../../assets/Waterfall.jpeg'


export default function AmazingPlaces() {
    return (
        <div className="amazingPlaces-conteiner">
            <div className="amazingPlaces-blockWithText">
                <p className="amazingPlaces-title">Удивительные места</p>
                <RedStripe />
                <p className="amazingPlaces-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit nullam nunc justo sagittis suscipit ultrices.</p>
            </div >
            <div className="amazingPlaces-conteinerWithImg">
                <div className="first-blockWhitImg">
                <img src={Road} className="img-firstBlock" />
                <img src={Waterfall} className="img-firstBlock" />
                </div>
                <div className="second-blockWhitImg">
                <img src={Mountains} className="img-seconBlock" />
                <img src={Venice} className="img-seconBlock" />
                <img src={RoadAndForest} className="img-seconBlock" />
                </div>
                <div className="third-blockWhitImg">
                <img src={RoadAndFog} className="img-thirdBlock" />
                <img src={Forest} className="img-thirdBlock" />
                </div>
            </div>
        </div>
    );
}