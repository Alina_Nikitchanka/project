import './Country.scss'
import Afrika from '../../assets/Afrika.jpeg';
import Bike from '../../assets/Bike.jpeg';
import Spain from '../../assets/Spain.jpeg';
import CityFromUp from '../../assets/CityFromUp.jpeg';
import Button from '../Button/Button';


export default function Country() {
    return (
        <div className="main-conteiner-country">
            <div className="country-conteiner">
                <div className="country-block">
                    <img src={Afrika} className="country-img"></img>
                    <p className="country-block_title">300 £ + скидки</p>
                    <p>От древних культур до удивительных пейзажей - найдите самые лучшие скидки</p>
                </div>
                <div className="country-block">
                    <img src={Bike} className="country-img"></img>
                    <p className="country-block_title">Велосипедные каникулы</p>
                    <p>Пробудите давно потерянное в детстве чувство свободы или бросьте вызов приключениям</p>
                </div>
                <div className="country-block">
                    <img src={Spain} className="country-img"></img>
                    <p className="country-block_title">Праздники Испании</p>
                    <p>Высокие горы, залитые солнцем побережья, мавританское наследие и более изысканная кухня</p>
                </div>
                <div className="country-block">
                    <img src={CityFromUp} className="country-img"></img>
                    <p className="country-block_title">Праздники Африки</p>
                    <p>Экзотические базары, древние чудеса, уникальная дикая природа и огромные песчаные дюны в бесконечных пустынях.</p>
                </div>
                <Button text="показать больше" />
            </div>
        </div>
    );
}