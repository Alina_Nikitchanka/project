import './Adventure.scss';
import Button from '../Button/Button';
import Balloons from '../../assets/Balloons.jpeg';
import Klyaksa from '../../assets/Klyaksa.png';
import OneBallon from '../../assets/OneBallon.png';



export default function Adventure() {
    return (
        <div className="sectione-adventure">
                <img src={Balloons} className="img-balloons" />
                <img src={Klyaksa} className="img-klyaksa" />
                <img src={OneBallon} className="img-oneBallon" />
                <p className="adventure-title">пришло время для великого </p>
                <p className="adventure-text">ПРИКЛЮЧЕНИЕ</p>
                <Button text="начать сейчас" />
        </div>
    );
}


